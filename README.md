## Automation testing over Orange HRM platform

---
Automation testing learning workshop using Cucumber BDD, Selenium and TestNg over
Orange HRM platform.

#### Tested features

- Login
- Logout
- Add employee
- Edit employee Job profile
- Assign supervisor to employee
- Find an employee
- Delete an employee
