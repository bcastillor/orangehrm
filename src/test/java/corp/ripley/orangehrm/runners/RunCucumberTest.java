package corp.ripley.orangehrm.runners;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.DataProvider;
import static io.cucumber.testng.CucumberOptions.SnippetType.CAMELCASE;

@CucumberOptions(
        features = { "classpath:features" },
        glue = { "corp.ripley.orangehrm.stepdefinitions" },
        tags = "",
        snippets = CAMELCASE,
        plugin = {
                "summary",
                "pretty",
                "json:target/cucumber/report.json",
                "html:target/cucumber/report.html",
                "junit:target/cucumber/report.xml"
        },
        monochrome = false,
        dryRun = false
)
public class RunCucumberTest extends AbstractTestNGCucumberTests {

    @Override
    @DataProvider(parallel = true)
    public Object[][] scenarios() {
        return super.scenarios();
    }
}
