package corp.ripley.orangehrm.utils;

import com.google.gson.Gson;
import corp.ripley.orangehrm.utils.exceptions.DatasourceTestPathNotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class JsonReader {

    private static final Logger log = LogManager.getLogger();

    /**
     * Método que lee obtiene la ruta base de los Datasources en formato JSON.
     * @return Retorna un objeto String con la ruta base de los Datasources JSON.
     */
    public static String getJsonDatasourcePathByEnvironment()  throws DatasourceTestPathNotFoundException {
        String testDataBasePath = PropertyReader.getGlobalProperties().getProperty("datasource.test.path");
        String env = System.getProperty("env", "qa");
        if(testDataBasePath == null) {
            throw new DatasourceTestPathNotFoundException();
        }
        return "%s/%s/json".formatted(testDataBasePath, env);
    }

    /**
     * Método que lee un archivo JSON y realiza un mapeo a partir de la clase template dada.
     * La clase template dada debe coincidir con el archivo en relación a sus propiedades.
     * @param templateClass Objecto generico Class<T>
     * @param fileName Objeto String que contiene el nombre sin extension del archivo a mapear.
     * @return Retorna un objeto generico T mapeado con la clase data como parametro.
     */
    public static <T> T readJsonFile(Class<T> templateClass, String fileName) {
        T jsonObject = null;
        try {
            String path = "%s/%s.json".formatted(getJsonDatasourcePathByEnvironment(), fileName);
            Gson gson = new Gson();
            BufferedReader bufferedReader = new BufferedReader(new FileReader(path));
            jsonObject = gson.fromJson(bufferedReader, templateClass);
        } catch (DatasourceTestPathNotFoundException | FileNotFoundException ex) {
            log.fatal(ex.getMessage());
        }
        return jsonObject;
    }
}
