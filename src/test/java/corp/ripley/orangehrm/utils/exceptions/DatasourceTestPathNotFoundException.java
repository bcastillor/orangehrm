package corp.ripley.orangehrm.utils.exceptions;

public class DatasourceTestPathNotFoundException extends Exception {

    public DatasourceTestPathNotFoundException() {
        super("Propiedad datasource.test.path no encontrada en archivo config.properties");
    }
}
