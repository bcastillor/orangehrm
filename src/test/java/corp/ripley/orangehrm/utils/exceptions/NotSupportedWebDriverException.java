package corp.ripley.orangehrm.utils.exceptions;

public class NotSupportedWebDriverException extends Exception {

    public NotSupportedWebDriverException(String webDriver) {
        super("Web driver: %s no soportado.".formatted(webDriver));
    }

}
