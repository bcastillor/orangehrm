package corp.ripley.orangehrm.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import corp.ripley.orangehrm.utils.exceptions.EnvironmentVariableNotFound;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PropertyReader {

    private static final Logger log = LogManager.getLogger();

    /**
     * Método que lee un archivo de propiedades dado.
     * @param fileName ruta del archivo de propiedades.
     * @return Retorna un objeto Properties con la colleccion de propiedades.
     */
    public static Properties readPropertyFile(String fileName) {
        Properties prop = null;
        try (FileInputStream fis = new FileInputStream(fileName)) {
            prop = new Properties();
            prop.load(fis);
        } catch(IOException ex) {
            log.fatal(ex.getMessage());
        }
        return prop;
    }

    /**
     * Método que obtiene las variables de propiedades de configuracion global: config.properties.
     * @return Retorna un objeto Properties con la colleccion de propiedades.
     */
    public static Properties getGlobalProperties() {
        String path = "src/test/resources/config.properties";
        return readPropertyFile(path);
    }

    /**
     * Método para cargar variables de datasource de basado en archivos de propiedades
     * dependiendo del ambiente parametrizado.
     * @return Retorna un objeto Properties con la colleccion de propiedades.
     */
    public static Properties loadDatasourcePropertiesPerEnvironment() throws EnvironmentVariableNotFound {
        String path = getGlobalProperties().getProperty("datasource.test.path");
        String dirPattern = "%s/%s/properties/%s";
        String env = System.getProperty("env", "qa");
        return switch (env) {
            case "qa" -> readPropertyFile(dirPattern.formatted(path, env, "dataqa.properties"));
            case "prod" -> readPropertyFile(dirPattern.formatted(path, env, "dataprod.properties"));
            default -> throw new EnvironmentVariableNotFound("No existe el ambiente: '%s'".formatted(env));
        };
    }

    /**
     * Método para obtener variables de los archivos properties
     * @return Retorna un objeto Properties con la colleccion de propiedades.
     */
    public static Properties getProperties() {
        Properties prop = new Properties();
        try {
            prop = loadDatasourcePropertiesPerEnvironment();
        } catch (EnvironmentVariableNotFound ex) {
            log.fatal(ex.getMessage());
        }
        return prop;
    }
}
