package corp.ripley.orangehrm.utils;

public class AssertionMessage {

    public static <T> String assertionMessage(String contexto, T valorEsperado, T valorActual) {
        return "%s: Se esperaba [%s], pero se encontro [%s].".formatted(contexto,
                String.valueOf(valorEsperado), String.valueOf(valorActual));
    }
}
