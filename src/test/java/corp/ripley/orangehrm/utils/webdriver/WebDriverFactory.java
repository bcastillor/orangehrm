package corp.ripley.orangehrm.utils.webdriver;

import corp.ripley.orangehrm.utils.exceptions.NotSupportedWebDriverException;
import org.openqa.selenium.WebDriver;
import corp.ripley.orangehrm.utils.PropertyReader;

/**
 * Clase Utilitaria que setea los drivers a utilizar para el Browser deseado según
 * el navegador definido en la propiedad de sistema "browser".
 * Por defecto el browser que se abrirá será chrome.
 */
public class WebDriverFactory {

    public static ThreadLocal<WebDriver> tlDriver = new ThreadLocal<>();

    /**
     * Crea un driver específico al navegador definido en la propiedad de sistema "browser".
     * Posteriormente devuelve el driver específico al Thread desde donde se llama.
     * @param nombreTest (Opcional) Nombre del Test o Escenario para determinar el nombre en Zalenium y Docker
     * @return Devuelve un WebDriver específico para un Thread.
     */
    public WebDriver createWebDriver(String ...nombreTest) throws NotSupportedWebDriverException {

        String testName = nombreTest.length > 0 ? nombreTest[0] : "";
        String webDriver = System.getProperty("browser", "chrome");

        switch (webDriver) {
            case "firefox":
                IWebDriver firefoxDriver = new FirefoxWebDriver();
                tlDriver.set(firefoxDriver.setUpWebDriver());
                return getDriver();

            case "chrome":
                IWebDriver chromeDriver = new ChromeWebDriver();
                tlDriver.set(chromeDriver.setUpWebDriver());
                return getDriver();

            case "chromeDocker":
                String chromeDockerRemoteUrl = PropertyReader
                        .getProperties()
                        .getProperty("chrome.docker.driver.remote.url");
                IWebDriver chromeDockerWebDriver = new ChromeDockerRemoteWebDriver(testName, chromeDockerRemoteUrl);
                tlDriver.set(chromeDockerWebDriver.setUpWebDriver());
                return getDriver();

            case "chromeZalenium":
                String chromeZaleniumRemoteUrl = PropertyReader
                        .getProperties()
                        .getProperty("chrome.zalenium.driver.remote.url");
                IWebDriver chromeZaleniumWebDriver = new ChromeZaleniumRemoteWebDriver(testName, chromeZaleniumRemoteUrl);
                tlDriver.set(chromeZaleniumWebDriver.setUpWebDriver());
                return getDriver();

            case "chromeBrowserStack":
                String username = System.getenv("BROWSERSTACK_USERNAME");
                String accessKey = System.getenv("BROWSERSTACK_ACCESS_KEY");
                String chromeBrowserStackRemoteUrl = PropertyReader
                        .getProperties()
                        .getProperty("chrome.browserstack.driver.remote.url");
                IWebDriver chromeBrowserStackWebDriver = new ChromeBrowserStackDriver(
                        testName, chromeBrowserStackRemoteUrl, username, accessKey);
                tlDriver.set(chromeBrowserStackWebDriver.setUpWebDriver());
                return getDriver();

            case "ie":
                IWebDriver ieDriver = new InternetExplorerWebDriver();
                tlDriver.set(ieDriver.setUpWebDriver());
                return getDriver();

            case "safari":
                IWebDriver safariDriver = new SafariWebDriver();
                tlDriver.set(safariDriver.setUpWebDriver());
                return getDriver();

            default:
                throw new NotSupportedWebDriverException(webDriver);
        }
    }

    /**
     * Método para obtener el driver creado con el método createWebDriver()
     * @return Retorna el driver específico según el Thread que creo el Driver.
     */
    public static  WebDriver getDriver() {
        return tlDriver.get();
    }

}
