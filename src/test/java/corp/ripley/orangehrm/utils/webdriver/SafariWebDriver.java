package corp.ripley.orangehrm.utils.webdriver;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.safari.SafariDriver;

public class SafariWebDriver implements IWebDriver {

    @Override
    public WebDriver setUpWebDriver() {
        WebDriverManager.safaridriver().setup();
        return new SafariDriver();
    }
}
