package corp.ripley.orangehrm.utils.webdriver;

import org.openqa.selenium.WebDriver;

public interface IWebDriver {

    WebDriver setUpWebDriver();
}
