package corp.ripley.orangehrm.utils.webdriver;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;

public class ChromeBrowserStackDriver implements IWebDriver {

    private static final Logger log = LogManager.getLogger();
    private final String testName;
    private final String url;
    private final DesiredCapabilities desiredCapabilities;

    public ChromeBrowserStackDriver(String testName, String url, String username, String accessKey) {
        url = url.replace("BROWSERSTACK_USERNAME", username);
        url = url.replace("BROWSERSTACK_ACCESS_KEY", accessKey);
        this.testName = testName;
        this.url = url;
        desiredCapabilities = new DesiredCapabilities();
    }

    @Override
    public WebDriver setUpWebDriver() {
        RemoteWebDriver remoteWebDriver = null;
        Hashtable<String, String> capsHashtable = new Hashtable<>();
        capsHashtable.put("browser", "chrome");
        capsHashtable.put("browser_version", "latest");
        capsHashtable.put("os", "Windows");
        capsHashtable.put("os_version", "10");
        capsHashtable.put("build", "browserstack-build-1");
        capsHashtable.put("name", testName);
        String key;
        // Iterate over the hashtable and set the capabilities
        Set<String> keys = capsHashtable.keySet();
        Iterator<String> itr = keys.iterator();
        while (itr.hasNext()) {
            key = itr.next();
            desiredCapabilities.setCapability(key, capsHashtable.get(key));
        }
        try {
            remoteWebDriver = new RemoteWebDriver(new URL(url), desiredCapabilities);
        } catch (MalformedURLException ex) {
            log.error(ex.getMessage());
        }
        return remoteWebDriver;
    }
}
