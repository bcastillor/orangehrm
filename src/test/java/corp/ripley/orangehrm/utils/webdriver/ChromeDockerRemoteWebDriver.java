package corp.ripley.orangehrm.utils.webdriver;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import java.net.MalformedURLException;
import java.net.URL;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ChromeDockerRemoteWebDriver implements IWebDriver {

    private static final Logger log = LogManager.getLogger();
    private final String testName;
    private final String url;
    private final ChromeOptions chromeDockerOpt;

    public ChromeDockerRemoteWebDriver(String testName, String url) {
        this.testName = testName;
        this.url = url;
        chromeDockerOpt = new ChromeOptions();
    }

    @Override
    public WebDriver setUpWebDriver() {
        RemoteWebDriver remoteWebDriver = null;
        WebDriverManager.chromedriver().setup();
        chromeDockerOpt.addArguments("--headless");
        chromeDockerOpt.addArguments("--disable-gpu");
        chromeDockerOpt.addArguments("--start-maximized");
        chromeDockerOpt.addArguments("--no-sandbox");
        chromeDockerOpt.addArguments("--ignore-certificate-errors");
        chromeDockerOpt.addArguments("--disable-popup-blocking");
        chromeDockerOpt.addArguments("--window-size=1920,1080");
        chromeDockerOpt.addArguments("--disable-dev-shm-usage");
        chromeDockerOpt.addArguments("--lang=es");
        chromeDockerOpt.setCapability("name", testName);
        try {
            remoteWebDriver = new RemoteWebDriver(new URL(url), chromeDockerOpt);
        } catch (MalformedURLException ex) {
            log.error(ex.getMessage());
        }
        return remoteWebDriver;
    }
}
