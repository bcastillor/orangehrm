package corp.ripley.orangehrm.datasource;

import corp.ripley.orangehrm.model.Credencial;
import corp.ripley.orangehrm.utils.JsonReader;
import java.util.Arrays;
import java.util.List;

public class CredencialDS {

    public static ThreadLocal<CredencialDS> threadLocal = ThreadLocal.withInitial(() -> new CredencialDS());

    private final List<Credencial> loginDataList;

    private CredencialDS() {
        // Private singleton constructor
        loginDataList = cargarCredencialesDesdeDatasource();
    }

    private List<Credencial> cargarCredencialesDesdeDatasource() {
        return Arrays.asList(JsonReader.readJsonFile(Credencial[].class, "Credencial"));
    }

    public Credencial buscarCredencialesValidasPorNombreDeUsuario(String username) {
        return loginDataList.stream()
                .filter( u -> u.getUsername().contentEquals(username) && u.isValid())
                .findAny()
                .get();
    }

    public Credencial buscarCredencialesInvalidasPorNombreDeUsuario(String username) {
        return loginDataList.stream()
                .filter( u -> u.getUsername().contentEquals(username) && !u.isValid())
                .findAny()
                .get();
    }

    public static CredencialDS getInstance() {
        return threadLocal.get();
    }


}
