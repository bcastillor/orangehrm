package corp.ripley.orangehrm.datasource;

import com.github.javafaker.Faker;
import corp.ripley.orangehrm.model.Empleado;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class EmpleadoDS {

    public static ThreadLocal<EmpleadoDS> threadLocal = ThreadLocal.withInitial(() -> new EmpleadoDS());

    public final List<Empleado> empleadoFakeList;

    private EmpleadoDS() {
        // Private singleton constructor
        empleadoFakeList = generarEmpleadoFakeList();
    }

    private static List<Empleado> generarEmpleadoFakeList() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        List<Empleado> empleadoFakeList = new ArrayList<>();
        for (int i = 0; i <= 9; i++) {
            Faker faker = new Faker();
            String idEmpleado = faker.number().digits(5);
            String primerNombre = faker.name().firstName();
            String segundoNombre = faker.name().firstName();
            String apellido = faker.name().lastName();
            int cargo = faker.number().numberBetween(1, 28);
            int estadoEmpleo = faker.number().numberBetween(1, 6);
            int categoriaTrabajo = faker.number().numberBetween(1, 9);
            String fechaIngreso = dateFormat.format(faker.date().past(faker.number().numberBetween(20, 30), TimeUnit.DAYS));
            int subUnidad = faker.number().numberBetween(1, 12);
            int ubicacion = faker.number().numberBetween(1, 4);
            String fechaInicio = dateFormat.format(faker.date().past(faker.number().numberBetween(5, 10), TimeUnit.DAYS));
            Empleado empleado = new Empleado(
                    idEmpleado, primerNombre, segundoNombre,
                    apellido, cargo, estadoEmpleo,
                    categoriaTrabajo, fechaIngreso,
                    subUnidad, ubicacion, fechaInicio);
            empleadoFakeList.add(empleado);
        }
        return empleadoFakeList;
    }

    public static EmpleadoDS getInstance() {
        return threadLocal.get();
    }

}
