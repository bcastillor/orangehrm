package corp.ripley.orangehrm.stepdefinitions;

import corp.ripley.orangehrm.pageobjects.DashboardPO;
import corp.ripley.orangehrm.pageobjects.LoginPO;
import corp.ripley.orangehrm.utils.AssertionMessage;
import corp.ripley.orangehrm.utils.JsonReader;
import corp.ripley.orangehrm.utils.PropertyReader;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import java.util.Arrays;
import java.util.List;

public class LoginSteps {

    private static final Logger log = LogManager.getLogger();
    private final LoginPO loginPO;
    private final DashboardPO dashboardPO;

    public LoginSteps(LoginPO loginPO, DashboardPO dashboardPO) {
        this.loginPO = loginPO;
        this.dashboardPO = dashboardPO;
    }

    @Given("estoy en la pagina login")
    public void estoyEnLaPaginaLogin() {
        loginPO.navigateToUrl(PropertyReader.getProperties().getProperty("orangehrm.url"));
    }

    @When("ingreso credenciales correctas de mi usuario")
    public void ingresoCredencialesCorrectasDeMiUsuario(DataTable table) {
        String username = table.cell(1, 0);
        loginPO.ingresarCredencialesCorrectas(username);
    }

    @Then("se despliega pagina de inicio con bienvenida")
    public void seDespliegaPaginaDeInicioConBienvedida(DataTable table) {
        String mensaje = table.cell(1, 0);
        String textoBienvenida = dashboardPO.obtenerMensajeDeBienvenida();

        Assert.assertTrue(
                textoBienvenida.contains(mensaje),
                AssertionMessage.assertionMessage("Mensaje de Bienvenida", mensaje, textoBienvenida)
        );
    }

    @And("se despliega menu inicio")
    public void seDespliegaMenuInicio() {
        log.debug("Obtener datos de prueba.");
        List<String> titulosMenuList = Arrays.asList(JsonReader.readJsonFile(String[].class, "Menu"));
        List<String> menu = dashboardPO.validarDepliegueDeMenu();
        String contextoAssertion = "Titulo menu";

        Assert.assertTrue(titulosMenuList.get(0).contentEquals(menu.get(0)),
                AssertionMessage.assertionMessage(contextoAssertion, titulosMenuList.get(0), menu.get(0)));

        Assert.assertTrue(titulosMenuList.get(1).contentEquals(menu.get(1)),
                AssertionMessage.assertionMessage(contextoAssertion, titulosMenuList.get(1), menu.get(1)));

        Assert.assertTrue(titulosMenuList.get(2).contentEquals(menu.get(2)),
                AssertionMessage.assertionMessage(contextoAssertion, titulosMenuList.get(2), menu.get(2)));

        Assert.assertTrue(titulosMenuList.get(3).contentEquals(menu.get(3)),
                AssertionMessage.assertionMessage(contextoAssertion, titulosMenuList.get(3), menu.get(3)));

        Assert.assertTrue(titulosMenuList.get(4).contentEquals(menu.get(4)),
                AssertionMessage.assertionMessage(contextoAssertion, titulosMenuList.get(4), menu.get(4)));

        Assert.assertTrue(titulosMenuList.get(5).contentEquals(menu.get(5)),
                AssertionMessage.assertionMessage(contextoAssertion, titulosMenuList.get(5), menu.get(5)));

        Assert.assertTrue(titulosMenuList.get(6).contentEquals(menu.get(6)),
                AssertionMessage.assertionMessage(contextoAssertion, titulosMenuList.get(6), menu.get(6)));

        Assert.assertTrue(titulosMenuList.get(7).contentEquals(menu.get(7)),
                AssertionMessage.assertionMessage(contextoAssertion, titulosMenuList.get(7), menu.get(7)));

        Assert.assertTrue(titulosMenuList.get(8).contentEquals(menu.get(8)),
                AssertionMessage.assertionMessage(contextoAssertion, titulosMenuList.get(8), menu.get(8)));

        Assert.assertTrue(titulosMenuList.get(9).contentEquals(menu.get(9)),
                AssertionMessage.assertionMessage(contextoAssertion, titulosMenuList.get(9), menu.get(9)));

        Assert.assertTrue(titulosMenuList.get(10).contentEquals(menu.get(10)),
                AssertionMessage.assertionMessage(contextoAssertion, titulosMenuList.get(10), menu.get(10)));
    }

    @When("ingreso credenciales fallidas de mi usuario {word}")
    public void ingresoCredencialesFallidasDeMiUsuario(String username) {
        loginPO.ingresarCredencialesIncorrectas(username);
    }

    @Then("se visualiza mensaje credenciales invalidas {string}")
    public void seVisualizaMensajeCredencialesInvalidas(String mensaje) {
        String textoCredencialesInvalidas = loginPO.obtenerMensajeCredencialesInvalidas();
        Assert.assertTrue(textoCredencialesInvalidas.contentEquals(mensaje));
    }

    @When("me deslogueo")
    public void meDeslogueo() {
        loginPO.clickEnBotonLogout();
    }

    @Then("se visualiza pagina de inicio")
    public void seVisualizaPaginaDeInicio() {
        boolean isShown = loginPO.seVisualizaTituloPanelLogin();
        Assert.assertTrue(isShown,
                "Visualizacion Panel Login: Se esperaba [true], pero se encontró [%s].".formatted(isShown));
    }
}
