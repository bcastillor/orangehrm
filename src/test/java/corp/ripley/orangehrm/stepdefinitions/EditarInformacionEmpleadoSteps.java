package corp.ripley.orangehrm.stepdefinitions;

import corp.ripley.orangehrm.datasource.EmpleadoDS;
import corp.ripley.orangehrm.model.Empleado;
import corp.ripley.orangehrm.pageobjects.EditEmployeeJobPO;
import corp.ripley.orangehrm.pageobjects.ReportToPO;
import corp.ripley.orangehrm.utils.AssertionMessage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import java.util.List;
import java.util.Map;

public class EditarInformacionEmpleadoSteps {

    private final EditEmployeeJobPO editEmployeeJobPO;
    private final ReportToPO reportToPO;

    public EditarInformacionEmpleadoSteps(EditEmployeeJobPO editEmployeeJobPO, ReportToPO reportToPO) {
        this.editEmployeeJobPO = editEmployeeJobPO;
        this.reportToPO = reportToPO;
    }

    @When("navego a perfil de trabajo")
    public void navegoAPerfilDeTrabajo() {
        editEmployeeJobPO.seleccionarOpcionMenuJob();
    }

    @And("edito perfil de trabajo de empleado {int}")
    public void editoPerfilDelTrabajo(int indexEmpleado) {
        editEmployeeJobPO.clickEnBotonEditarGuardar();
        editEmployeeJobPO.editarFormularioJob(indexEmpleado);
    }

    @And("guardo el formulario de trabajo")
    public void guardoElFormularioDeTrabajo() {
        editEmployeeJobPO.clickEnBotonEditarGuardar();
    }

    @Then("se visualiza la data actualizada de empleado {int}")
    public void seVisualizaLaDataActualizada(int indexEmpleado) {
        EmpleadoDS empleadoDS = EmpleadoDS.getInstance();
        Empleado empleadoFake = empleadoDS.empleadoFakeList.get(indexEmpleado);
        Map<String, Object> datosFormulario = editEmployeeJobPO.obtenerCambiosGuardadosDelFormulario();
        int indexSelectorCargo = (int) datosFormulario.get("indexSelectorCargo");
        int indexSelectorEstadoEmpleo = (int) datosFormulario.get("indexSelectorEstadoEmpleo");
        int indexSelectorCategoriaTrabajo = (int) datosFormulario.get("indexelectorCategoriaTrabajo");
        String textoCampofechaIngreso = (String) datosFormulario.get("textoCampofechaIngreso");
        int indexSelectorSubUnidad = (int) datosFormulario.get("indexSelectorSubUnidad");
        int indexSelectorUbicacion = (int) datosFormulario.get("indexSelectorUbicacion");
        String textoCampoFechaInicio = (String) datosFormulario.get("textoCampoFechaInicio");
        String contextAssertion = "Edicion formulario trabajo";

        Assert.assertTrue(
                empleadoFake.getCargo() == indexSelectorCargo,
                AssertionMessage.assertionMessage(contextAssertion, empleadoFake.getCargo(), indexSelectorCargo)
        );

        Assert.assertTrue(
                empleadoFake.getEstadoEmpleo() == indexSelectorEstadoEmpleo,
                AssertionMessage.assertionMessage(contextAssertion, empleadoFake.getEstadoEmpleo(), indexSelectorEstadoEmpleo)
        );

        Assert.assertTrue(
                empleadoFake.getCategoriaTrabajo() == indexSelectorCategoriaTrabajo,
                AssertionMessage.assertionMessage(contextAssertion, empleadoFake.getCategoriaTrabajo(), indexSelectorCategoriaTrabajo)
        );

        Assert.assertTrue(
                empleadoFake.getFechaIngreso().contentEquals(textoCampofechaIngreso),
                AssertionMessage.assertionMessage(contextAssertion, empleadoFake.getFechaIngreso(), textoCampofechaIngreso)
        );

        Assert.assertTrue(
                empleadoFake.getSubUnidad() == indexSelectorSubUnidad,
                AssertionMessage.assertionMessage(contextAssertion, empleadoFake.getSubUnidad(), indexSelectorSubUnidad)
        );

        Assert.assertTrue(
                empleadoFake.getUbicacion() == indexSelectorUbicacion,
                AssertionMessage.assertionMessage(contextAssertion, empleadoFake.getUbicacion(), indexSelectorUbicacion)
        );

        Assert.assertTrue(
                empleadoFake.getFechaInicio().contentEquals(textoCampoFechaInicio),
                AssertionMessage.assertionMessage(contextAssertion, empleadoFake.getFechaInicio(), textoCampoFechaInicio)
        );
    }

    @Given("navego a perfil reportar")
    public void navegoAPerfilReportar() {
        reportToPO.irAPaginaReportTo();
        reportToPO.irAlFormularioAgregarSupervisor();
    }

    @When("asigno supervisor {string} con metodo {string} a empleado {int}")
    public void asignoSupervisorSupervisorNameConMetodoReportingMethod(
            String supervisorName, String reportingMethod, int indexEmpleado) {
        reportToPO.llenarFormularioAgregarSupervisor(supervisorName, reportingMethod, indexEmpleado);
        reportToPO.guardarFormularioAgregarSupervisor();
    }

    @Then("se visualiza en la tabla el supervisor {string} y metodo {string}")
    public void seVisualizaEnLaTablaElSupervisorSupervisorNameYMetodoReportingMethod(
            String supervisorName, String reportingMethod) {
        List<String> supervisorAsignado = reportToPO.obtenerSupervisorAsignadoEnTabla(supervisorName);
        String contextoAssertion = "Supervisor asignado";

        Assert.assertTrue(
                supervisorName.contentEquals(supervisorAsignado.get(1)),
                AssertionMessage.assertionMessage(contextoAssertion, supervisorName, supervisorAsignado.get(1))
        );

        Assert.assertTrue(
                reportingMethod.contentEquals(supervisorAsignado.get(2)),
                AssertionMessage.assertionMessage(contextoAssertion, reportingMethod, supervisorAsignado.get(2))
        );
    }
}
