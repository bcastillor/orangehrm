package corp.ripley.orangehrm.stepdefinitions;

import corp.ripley.orangehrm.datasource.EmpleadoDS;
import corp.ripley.orangehrm.model.Empleado;
import corp.ripley.orangehrm.pageobjects.EmployeeListPO;
import corp.ripley.orangehrm.utils.AssertionMessage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import java.util.Map;

public class BuscarEmpleadoSteps {

    private final EmployeeListPO employeeListPO;

    public BuscarEmpleadoSteps(EmployeeListPO employeeListPO) {
        this.employeeListPO = employeeListPO;
    }

    private Empleado obtenerEmpleadoFake(int indexEmpleado) {
        EmpleadoDS empleadoDS = EmpleadoDS.getInstance();
        return empleadoDS.empleadoFakeList.get(indexEmpleado);
    }

    @When("navego a la pagina Lista Empleados")
    public void navegoALaPaginaListaEmpleados() {
        employeeListPO.irAPaginaEmployeeList();
    }

    @And("busco por el nombre completo del empleado {int}")
    public void buscoPorElNombreCompletoDelEmpleado(int indexEmleado) {
        Empleado empleadoFake = obtenerEmpleadoFake(indexEmleado);
        employeeListPO.ingresarNombreCompletoDeUsuario(empleadoFake);
        employeeListPO.clickEnBotonBuscar();
    }

    @Then("se muestra empleado {int} en la tabla de resultados")
    public void seMuestraEmpleadoEnLaTablaDeResultados(int indexEmpleado) {
        EmpleadoDS empleadoDS = EmpleadoDS.getInstance();
        Empleado empleadoFake = empleadoDS.empleadoFakeList.get(indexEmpleado);
        String nombresEmpleadoFake = "%s %s".formatted(empleadoFake.getPrimerNombre(),
                empleadoFake.getSegundoNombre());

        Map<String, Object> datosEmpleado = employeeListPO.obtenerDatosDeUsuarioDesdeTabla(empleadoFake);
        String primerYSegundoNombre = (String) datosEmpleado.get("primerYSegundoNombre");
        String apellido = (String) datosEmpleado.get("apellido");
        int cargo = (int) datosEmpleado.get("cargo");
        int estadoEmpleo = (int) datosEmpleado.get("estadoEmpleo");
        int subUnidad = (int) datosEmpleado.get("subUnidad");
        String supervisor = (String) datosEmpleado.get("supervisor");
        String contextoAssertion = "Resultado busqueda usuario";

        Assert.assertTrue(
                nombresEmpleadoFake.contentEquals(primerYSegundoNombre),
                AssertionMessage.assertionMessage(contextoAssertion, nombresEmpleadoFake, primerYSegundoNombre)
        );

        Assert.assertTrue(
                empleadoFake.getApellido().contentEquals(apellido),
                AssertionMessage.assertionMessage(contextoAssertion, empleadoFake.getApellido(), apellido)
        );

        Assert.assertTrue(
                empleadoFake.getCargo() == cargo,
                AssertionMessage.assertionMessage(contextoAssertion, empleadoFake.getCargo(), cargo)
        );

        Assert.assertTrue(
                empleadoFake.getEstadoEmpleo() == estadoEmpleo,
                AssertionMessage.assertionMessage(contextoAssertion, empleadoFake.getEstadoEmpleo(), estadoEmpleo)
        );

        Assert.assertTrue(
                empleadoFake.getSubUnidad() == subUnidad,
                AssertionMessage.assertionMessage(contextoAssertion, empleadoFake.getSubUnidad(), subUnidad)
        );

        Assert.assertTrue(
                empleadoFake.getSupervisor().contentEquals(supervisor),
                AssertionMessage.assertionMessage(contextoAssertion, empleadoFake.getSupervisor(), supervisor)
        );
    }

    @When("elimino empleado {int}")
    public void eliminoEmpleado(int indexEmpleado) {
        Empleado empleadoFake = obtenerEmpleadoFake(indexEmpleado);
        employeeListPO.marcarCheckboxUsuarioEncontrado(empleadoFake);
        employeeListPO.clickEnBotonDelete();
        employeeListPO.confirmarEliminacionDeEmpleado();

    }

    @Then("se muestra mensaje {string}")
    public void seMuestraMensaje(String mensaje) {
        String textoNoRecords = employeeListPO.obtenerMensajeSinRegistrosEnLaTabla();
        String contextAssertion = "Mensaje sin registro en tabla usuarios";
        Assert.assertTrue(
                mensaje.contentEquals(textoNoRecords),
                AssertionMessage.assertionMessage(contextAssertion, mensaje, textoNoRecords)
        );
    }
}
