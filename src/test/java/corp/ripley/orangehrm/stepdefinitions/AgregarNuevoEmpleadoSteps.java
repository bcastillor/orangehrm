package corp.ripley.orangehrm.stepdefinitions;

import corp.ripley.orangehrm.datasource.EmpleadoDS;
import corp.ripley.orangehrm.model.Empleado;
import corp.ripley.orangehrm.pageobjects.AddEmployeePO;
import corp.ripley.orangehrm.pageobjects.PersonalDetailsPO;
import corp.ripley.orangehrm.utils.AssertionMessage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import java.util.Map;

public class AgregarNuevoEmpleadoSteps {


    private final AddEmployeePO addEmployeePO;
    private final PersonalDetailsPO personalDetailsPO;

    public AgregarNuevoEmpleadoSteps(AddEmployeePO addEmployeePO,
                                     PersonalDetailsPO personalDetailsPO) {

        this.addEmployeePO = addEmployeePO;
        this.personalDetailsPO = personalDetailsPO;
    }

    @When("navego a la pagina agregar empleado")
    public void navegoAlaPaginaAgregarEmpleado() {
        addEmployeePO.irAPaginaAddEmployee();
    }

    @And("ingreso un nuevo empleado {int}")
    public void ingresounNuevoEmpleado(int indexEmpleado) {
        addEmployeePO.llenarFormulario(indexEmpleado);
        addEmployeePO.clickBotonSave();
    }

    @Then("se despliega pagina con informacion del empleado {int} ingresado")
    public void seDespliegaPaginaConInformacionDelEmpleadoIngresado(int indexEmpleado) {
        EmpleadoDS empleadoDS = EmpleadoDS.getInstance();
        Empleado empleadoFake = empleadoDS.empleadoFakeList.get(indexEmpleado);
        Map<String, String> camposEmpleado = personalDetailsPO.validarDatosIngresados();
        String campoIdEmpleado = camposEmpleado.get("campoIdEmpleado");
        String campoPrimerNombre = camposEmpleado.get("campoPrimerNombre");
        String campoSegundoNombre = camposEmpleado.get("campoSegundoNombre");
        String campoApellido = camposEmpleado.get("campoApellido");
        String contextoAssertion = "Informacion Perfil Empleado";

        Assert.assertTrue(
                campoIdEmpleado.contentEquals(empleadoFake.getIdEmpleado()),
                AssertionMessage.assertionMessage(contextoAssertion, empleadoFake.getIdEmpleado(), campoIdEmpleado)
        );
        Assert.assertTrue(
                campoPrimerNombre.contentEquals(empleadoFake.getPrimerNombre()),
                AssertionMessage.assertionMessage(contextoAssertion, empleadoFake.getPrimerNombre(), campoPrimerNombre)
        );
        Assert.assertTrue(
                campoSegundoNombre.contentEquals(empleadoFake.getSegundoNombre()),
                AssertionMessage.assertionMessage(contextoAssertion, empleadoFake.getSegundoNombre(), campoSegundoNombre)
        );
        Assert.assertTrue(
                campoApellido.contentEquals(empleadoFake.getApellido()),
                AssertionMessage.assertionMessage(contextoAssertion, empleadoFake.getApellido(), campoApellido)
        );
    }
}
