package corp.ripley.orangehrm.model;

public class Empleado {

    private String idEmpleado;
    private String primerNombre;
    private String segundoNombre;
    private String apellido;
    private int cargo;
    private int estadoEmpleo;
    private int categoriaTrabajo;
    private String fechaIngreso;
    private int subUnidad;
    private int ubicacion;
    private String fechaInicio;
    private String supervisor;

    public Empleado(String idEmpleado, String primerNombre, String segundoNombre,
                    String apellido, int cargo, int estadoEmpleo,
                    int categoriaTrabajo, String fechaIngreso, int subUnidad,
                    int ubicacion, String fechaInicio) {
        this.idEmpleado = idEmpleado;
        this.primerNombre = primerNombre;
        this.segundoNombre = segundoNombre;
        this.apellido = apellido;
        this.cargo = cargo;
        this.estadoEmpleo = estadoEmpleo;
        this.categoriaTrabajo = categoriaTrabajo;
        this.fechaIngreso = fechaIngreso;
        this.subUnidad = subUnidad;
        this.ubicacion = ubicacion;
        this.fechaInicio = fechaInicio;
    }

    public String getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(String idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getCargo() {
        return cargo;
    }

    public void setCargo(int cargo) {
        this.cargo = cargo;
    }

    public int getEstadoEmpleo() {
        return estadoEmpleo;
    }

    public void setEstadoEmpleo(int estadoEmpleo) {
        this.estadoEmpleo = estadoEmpleo;
    }

    public int getCategoriaTrabajo() {
        return categoriaTrabajo;
    }

    public void setCategoriaTrabajo(int categoriaTrabajo) {
        this.categoriaTrabajo = categoriaTrabajo;
    }

    public String getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public int getSubUnidad() {
        return subUnidad;
    }

    public void setSubUnidad(int subUnidad) {
        this.subUnidad = subUnidad;
    }

    public int getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(int ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(String supervisor) {
        this.supervisor = supervisor;
    }
}
