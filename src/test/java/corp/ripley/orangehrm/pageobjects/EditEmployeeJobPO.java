package corp.ripley.orangehrm.pageobjects;

import corp.ripley.orangehrm.datasource.EmpleadoDS;
import corp.ripley.orangehrm.model.Empleado;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.HashMap;
import java.util.Map;

public class EditEmployeeJobPO extends BasePage {

    @FindBy(xpath = "//ul[@id='sidenav']/li/a[text() = 'Job']")
    private WebElement menuLateralJob;

    @FindBy(id = "btnSave")
    private WebElement botonEditarGuardar;

    @FindBy(id = "job_job_title")
    private WebElement selectorCargo;

    @FindBy(id = "job_emp_status")
    private WebElement selectorEstadoEmpleo;

    @FindBy(id = "job_eeo_category")
    private WebElement selectorCategoriaTrabajo;

    @FindBy(id = "job_joined_date")
    private WebElement campofechaIngreso;

    @FindBy(id = "ui-datepicker-div")
    private WebElement selectorFechaIngreso;

    @FindBy(id = "job_sub_unit")
    private WebElement selectorSubUnidad;

    @FindBy(id = "job_location")
    private WebElement selectorUbicacion;

    @FindBy(id = "job_contract_start_date")
    private WebElement campoFechaInicio;

    public void seleccionarOpcionMenuJob() {
        log.debug("Seleccionar menu Job.");
        click(menuLateralJob);
    }

    public void clickEnBotonEditarGuardar() {
        log.debug("Click en boton Edit/Save.");
        click(botonEditarGuardar);
    }

    public void editarFormularioJob(int indexEmpleado) {
        log.debug("Editar formulario de Trabajo.");
        EmpleadoDS empleadoDS = EmpleadoDS.getInstance();
        Empleado empleado = empleadoDS.empleadoFakeList.get(indexEmpleado);
        selectOptionByIndex(selectorCargo, empleado.getCargo());
        selectOptionByIndex(selectorEstadoEmpleo, empleado.getEstadoEmpleo());
        selectOptionByIndex(selectorCategoriaTrabajo, empleado.getCategoriaTrabajo());
        sendKeys(campofechaIngreso, empleado.getFechaIngreso());
        sendKeys(campofechaIngreso, Keys.ENTER);
        waitForElementToBeInvisible(selectorFechaIngreso);
        selectOptionByIndex(selectorSubUnidad, empleado.getSubUnidad());
        selectOptionByIndex(selectorUbicacion, empleado.getUbicacion());
        sendKeys(campoFechaInicio, empleado.getFechaInicio());
    }

    public Map<String, Object> obtenerCambiosGuardadosDelFormulario() {
        log.debug("Obtener cambios guardados en formulario de Trabajo.");
        Map<String, Object> datosFormulario = new HashMap<>();
        datosFormulario.put("indexSelectorCargo", getSelectedOptionIndex(selectorCargo));
        datosFormulario.put("indexSelectorEstadoEmpleo", getSelectedOptionIndex(selectorEstadoEmpleo));
        datosFormulario.put("indexelectorCategoriaTrabajo", getSelectedOptionIndex(selectorCategoriaTrabajo));
        datosFormulario.put("textoCampofechaIngreso", getAttributeValue(campofechaIngreso, "value"));
        datosFormulario.put("indexSelectorSubUnidad", getSelectedOptionIndex(selectorSubUnidad));
        datosFormulario.put("indexSelectorUbicacion", getSelectedOptionIndex(selectorUbicacion));
        datosFormulario.put("textoCampoFechaInicio", getAttributeValue(campoFechaInicio, "value"));
        return datosFormulario;
    }

}
