package corp.ripley.orangehrm.pageobjects;

import corp.ripley.orangehrm.utils.PropertyReader;
import corp.ripley.orangehrm.utils.webdriver.WebDriverFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;
import java.util.List;

public class BasePage {

    protected WebDriver driver;
    protected WebDriverWait wait;
    protected static final Logger log = LogManager.getLogger();

    private static final int TIMEOUT = Integer.parseInt(PropertyReader
            .getGlobalProperties()
            .getProperty("webdriver.timeout"));

    private static final int POLLING = Integer.parseInt(PropertyReader
            .getGlobalProperties()
            .getProperty("webdriver.polling"));

    /**
     * Constructor de clase en el cual se setea un wait de elementos, el WebDriver
     * e inicializa los elementos de una clase Page usando initElements de PageFactory
     */
    public BasePage() {
        driver = WebDriverFactory.getDriver();
        wait = new WebDriverWait(this.driver, Duration.ofSeconds(TIMEOUT),
                Duration.ofMillis(POLLING));
        PageFactory.initElements(new AjaxElementLocatorFactory(this.driver, TIMEOUT), this);
    }

    public void navigateToUrl(String url) {
        driver.get(url);
    }

    public void sendKeys(WebElement webElement, String text) {
        wait.until(ExpectedConditions.elementToBeClickable(webElement));
        webElement.clear();
        webElement.sendKeys(text);
    }

    public void sendKeys(WebElement webElement, Keys key) {
        wait.until(ExpectedConditions.elementToBeClickable(webElement));
        webElement.sendKeys(key);
    }

    public void click(WebElement webElement) {
        wait.until(ExpectedConditions.elementToBeClickable(webElement)).click();
    }

    public String getText(WebElement webElement) {
        wait.until(ExpectedConditions.visibilityOf(webElement));
        return webElement.getText();
    }

    public String getAttributeValue(WebElement webElement, String attributeName) {
        wait.until(ExpectedConditions.visibilityOf(webElement));
        return webElement.getAttribute(attributeName);
    }

    public void selectOptionByIndex(WebElement webElement, int index) {
        wait.until(ExpectedConditions.visibilityOf(webElement));
        Select select = new Select(webElement);
        select.selectByIndex(index);
    }

    public int getSelectedOptionIndex(WebElement webElement) {
        wait.until(ExpectedConditions.visibilityOf(webElement));
        Select select = new Select(webElement);
        return select.getOptions().indexOf(select.getFirstSelectedOption());
    }

    public void selectOptionByText(WebElement webElement, String text) {
        wait.until(ExpectedConditions.visibilityOf(webElement));
        Select select = new Select(webElement);
        select.selectByVisibleText(text);
    }

    public String getSelectedOptionText(WebElement webElement) {
        wait.until(ExpectedConditions.visibilityOf(webElement));
        Select select = new Select(webElement);
        return select.getFirstSelectedOption().getText();
    }

    public void selectOptionByValue(WebElement webElement, String value) {
        wait.until(ExpectedConditions.visibilityOf(webElement));
        Select select = new Select(webElement);
        select.selectByValue(value);
    }

    public String getSelectedOptionValue(WebElement webElement) {
        wait.until(ExpectedConditions.visibilityOf(webElement));
        Select select = new Select(webElement);
        return select.getFirstSelectedOption().getAttribute("value");
    }

    public String getOptionTextByIndex(WebElement webElement, int index) {
        wait.until(ExpectedConditions.visibilityOf(webElement));
        Select select = new Select(webElement);
        return select.getOptions().get(index).getText();
    }

    public int getOptionIndexByText(WebElement webElement, String text) {
        wait.until(ExpectedConditions.visibilityOf(webElement));
        Select select = new Select(webElement);
        List<WebElement> optionList = select.getOptions();
        int index = -1;
        for (int i = 0; i < optionList.size(); i++) {
            if(optionList.get(i).getText().trim().contentEquals(text.trim())) {
                index = i;
                break;
            }
        }
        return index;
    }

    public void waitForElement(WebElement webElement) {
        wait.until(ExpectedConditions.visibilityOf(webElement));
    }

    public void waitForElementToBeInvisible(WebElement webElement) {
        wait.until(ExpectedConditions.invisibilityOf(webElement));
    }

    public boolean isDisplayed(WebElement webElement) {
        return wait.until(ExpectedConditions.visibilityOf(webElement)).isDisplayed();
    }

    public boolean isDisplayedAndClickable(WebElement webElement) {
        return wait.until(ExpectedConditions.elementToBeClickable(webElement)).isDisplayed();
    }
}
