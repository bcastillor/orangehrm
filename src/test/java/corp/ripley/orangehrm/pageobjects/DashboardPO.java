package corp.ripley.orangehrm.pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.ArrayList;
import java.util.List;

public class DashboardPO extends BasePage  {

    @FindBy(id = "welcome")
    private WebElement textoBienvenida;

    @FindBy(xpath = "//a[@id='menu_admin_viewAdminModule']/b")
    private WebElement menuAdmin;

    @FindBy(xpath = "//a[@id='menu_pim_viewPimModule']/b")
    private WebElement menuPIM;

    @FindBy(xpath = "//a[@id='menu_leave_viewLeaveModule']/b")
    private WebElement menuLeave;

    @FindBy(xpath = "//a[@id='menu_time_viewTimeModule']/b")
    private WebElement menuTime;

    @FindBy(xpath = "//a[@id='menu_recruitment_viewRecruitmentModule']/b")
    private WebElement menuRecruitment;

    @FindBy(xpath = "//a[@id='menu_pim_viewMyDetails']/b")
    private WebElement menuMyInfo;

    @FindBy(xpath = "//a[@id='menu__Performance']/b")
    private WebElement menuPerformance;

    @FindBy(xpath = "//a[@id='menu_dashboard_index']/b")
    private WebElement menuDashboard;

    @FindBy(xpath = "//a[@id='menu_directory_viewDirectory']/b")
    private WebElement menuDirectory;

    @FindBy(xpath = "//a[@id='menu_maintenance_purgeEmployee']/b")
    private WebElement menuMaintenance;

    @FindBy(xpath = "//a[@id='menu_buzz_viewBuzz']/b")
    private WebElement menuBuzz;

    public String obtenerMensajeDeBienvenida() {
        log.debug("Obtener texto de Bienvenida al usuario.");
        return getText(textoBienvenida);
    }

    public List<String> validarDepliegueDeMenu() {
        log.debug("Obtener titulos del menu inicio.");
        List<String> menu = new ArrayList<>();
        menu.add(getText(menuAdmin));
        menu.add(getText(menuPIM));
        menu.add(getText(menuLeave));
        menu.add(getText(menuTime));
        menu.add(getText(menuRecruitment));
        menu.add(getText(menuMyInfo));
        menu.add(getText(menuPerformance));
        menu.add(getText(menuDashboard));
        menu.add(getText(menuDirectory));
        menu.add(getText(menuMaintenance));
        menu.add(getText(menuBuzz));
        return menu;
    }
}
