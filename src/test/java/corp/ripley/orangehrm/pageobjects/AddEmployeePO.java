package corp.ripley.orangehrm.pageobjects;

import corp.ripley.orangehrm.datasource.EmpleadoDS;
import corp.ripley.orangehrm.model.Empleado;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AddEmployeePO extends BasePage {

    @FindBy(id = "menu_pim_viewPimModule")
    private WebElement menuPIM;

    @FindBy(id = "menu_pim_addEmployee")
    private WebElement menuAddEmployee;

    @FindBy(id="employeeId")
    private WebElement campoIdEmpleado;

    @FindBy(id = "firstName")
    private WebElement campoPrimerNombre;

    @FindBy(id = "middleName")
    private WebElement campoSegundoNombre;

    @FindBy(id = "lastName")
    private WebElement campoApellido;

    @FindBy(id = "btnSave")
    private WebElement botonSave;

    public void irAPaginaAddEmployee() {
        log.debug("Navegar a la pagina 'Agregar Empleado'.");
        click(menuPIM);
        click(menuAddEmployee);
    }

    public void llenarFormulario(int indexEmpleado) {
        log.debug("Llenar formulario para agregar empleado.");
        EmpleadoDS empleadoDS = EmpleadoDS.getInstance();
        Empleado empleado = empleadoDS.empleadoFakeList.get(indexEmpleado);
        sendKeys(campoIdEmpleado, empleado.getIdEmpleado());
        sendKeys(campoPrimerNombre, empleado.getPrimerNombre());
        sendKeys(campoSegundoNombre, empleado.getSegundoNombre());
        sendKeys(campoApellido, empleado.getApellido());
    }

    public void clickBotonSave() {
        log.debug("Click en boton Save.");
        click(botonSave);
    }
}
