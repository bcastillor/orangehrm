package corp.ripley.orangehrm.pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.HashMap;
import java.util.Map;

public class PersonalDetailsPO extends BasePage {

    @FindBy(id="personal_txtEmployeeId")
    private WebElement campoIdEmpleado;

    @FindBy(id = "personal_txtEmpFirstName")
    private WebElement campoPrimerNombre;

    @FindBy(id = "personal_txtEmpMiddleName")
    private WebElement campoSegundoNombre;

    @FindBy(id = "personal_txtEmpLastName")
    private WebElement campoApellido;

    public Map<String, String> validarDatosIngresados() {
        log.debug("Obtener datos de empleado ingresado.");
        Map<String, String> camposEmpleado = new HashMap<>();
        camposEmpleado.put("campoIdEmpleado", getAttributeValue(campoIdEmpleado, "value"));
        camposEmpleado.put("campoPrimerNombre", getAttributeValue(campoPrimerNombre, "value"));
        camposEmpleado.put("campoSegundoNombre", getAttributeValue(campoSegundoNombre, "value"));
        camposEmpleado.put("campoApellido", getAttributeValue(campoApellido, "value"));
        return camposEmpleado;
    }
}
