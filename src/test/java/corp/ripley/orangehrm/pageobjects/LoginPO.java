package corp.ripley.orangehrm.pageobjects;

import corp.ripley.orangehrm.datasource.CredencialDS;
import corp.ripley.orangehrm.model.Credencial;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPO extends BasePage {

    @FindBy(id = "txtUsername")
    private WebElement campoUsername;

    @FindBy(id = "txtPassword")
    private WebElement campoPassword;

    @FindBy(id = "btnLogin")
    private WebElement botonLogin;

    @FindBy(xpath = "//div[@id='divLoginButton']/span[@id='spanMessage']")
    private WebElement mensajeCredencialesInvalidas;

    @FindBy(id = "welcome")
    private WebElement botonWelcomeUsuario;

    @FindBy(id = "welcome-menu")
    private WebElement menuWelcome;

    @FindBy(xpath = "//div[@id='welcome-menu']/ul/li[3]/a[text() = 'Logout']")
    private WebElement botonLogout;

    @FindBy(id = "logInPanelHeading")
    private WebElement tituloPanelLogin;

    public Credencial obtenerCredencialesValidasPorNombreDeUsuario(String username) {
        CredencialDS credencialDS = CredencialDS.getInstance();
        Credencial credencial = credencialDS.buscarCredencialesValidasPorNombreDeUsuario(username);
        return credencial;
    }

    public Credencial obtenerCredencialesInvalidasPorNombreDeUsuario(String username) {
        CredencialDS credencialDS = CredencialDS.getInstance();
        Credencial credencial = credencialDS.buscarCredencialesInvalidasPorNombreDeUsuario(username);
        return credencial;
    }

    public void ingresarCredencialesEnFormulario(String username, String password) {
        sendKeys(campoUsername, username);
        sendKeys(campoPassword, password);
    }

    public void clickBotonLogin() {
        log.debug("Click en boton login.");
        click(botonLogin);
    }

    public void ingresarCredencialesCorrectas(String username) {
        Credencial credencial = obtenerCredencialesValidasPorNombreDeUsuario(username);
        String password= credencial.getPassword();
        log.debug("Ingreso de credenciales correctas.");
        ingresarCredencialesEnFormulario(username, password);
        clickBotonLogin();
    }

    public void ingresarCredencialesIncorrectas(String username) {
        Credencial credencial = obtenerCredencialesInvalidasPorNombreDeUsuario(username);
        String password = credencial.getPassword();
        log.debug("Ingreso de credenciales correctas.");
        ingresarCredencialesEnFormulario(username, password);
        clickBotonLogin();
    }

    public String obtenerMensajeCredencialesInvalidas() {
        return getText(mensajeCredencialesInvalidas);

    }

    public void clickEnBotonLogout() {
        log.debug("Click en boton Logout.");
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].click();", botonWelcomeUsuario);
        click(botonLogout);
    }

    public boolean seVisualizaTituloPanelLogin() {
        return isDisplayed(tituloPanelLogin);
    }
}
