package corp.ripley.orangehrm.pageobjects;

import corp.ripley.orangehrm.model.Empleado;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EmployeeListPO extends BasePage {

    @FindBy(xpath = "//a[@id='menu_pim_viewPimModule']/b")
    private WebElement menuPIM;

    @FindBy(id = "menu_pim_viewEmployeeList")
    private WebElement menuListaEmpleado;

    @FindBy(xpath = "//div[@id='employee-information']/div/h1[text() = 'Employee Information']")
    private WebElement toggleInformacionEmpleado;

    @FindBy(id = "empsearch_employee_name_empName")
    private WebElement campoBusquedaNombreEmpleado;

    @FindBy(id = "searchBtn")
    private WebElement botonBuscar;

    @FindBy(id = "btnDelete")
    private WebElement botonDelete;

    @FindBy(id = "dialogDeleteBtn")
    private WebElement botonConfirmarDelete;

    @FindBy(xpath = "//table[@id='resultTable']/tbody/tr")
    private List<WebElement> filasTablaEmpleados;

    @FindBy(id = "empsearch_job_title")
    private WebElement selectorJobTitle;

    @FindBy(id = "empsearch_employee_status")
    private WebElement selectorEmploymentStatus;

    @FindBy(id = "empsearch_sub_unit")
    private WebElement selectorSubUnit;

    @FindBy(xpath = "//div[@id='search-results']/div/form[@id='frmList_ohrmListComponent']/div/ul[@class='paging top']")
    private WebElement paginador;

    public List<List<WebElement>> obtenerFilasDeTablaEmpleados() {
        log.debug("Obtener datos de tabla de 'Empleados'.");
        List<List<WebElement>> tabla = new ArrayList<>();
        for(WebElement filaHtml: filasTablaEmpleados) {
            List<WebElement> filaList = new ArrayList<>();
            for(WebElement columnaHtml : filaHtml.findElements(By.xpath("./td"))) {
                filaList.add(columnaHtml);
            }
            tabla.add(filaList);
        }
        return tabla;
    }

    public void irAPaginaEmployeeList() {
        log.debug("Navegar a la pagina 'Lista de Empleados'");
        click(menuPIM);
        click(menuListaEmpleado);
        isDisplayed(toggleInformacionEmpleado);
    }

    public void ingresarNombreCompletoDeUsuario(Empleado empleadoFake) {
        log.debug("Ingresar nombre completo del empleado.");
        String nombreCompleto = "%s %s %s".formatted(empleadoFake.getPrimerNombre(),
                empleadoFake.getSegundoNombre(), empleadoFake.getApellido());
        wait.until((ExpectedCondition<Boolean>) webDriver -> {
            String campoListoParaIngresarTexto = getAttributeValue(campoBusquedaNombreEmpleado, "class");
            if(campoListoParaIngresarTexto.contentEquals("ac_input inputFormatHint")) {
                return true;
            } else {
                return false;
            }
        });
        sendKeys(campoBusquedaNombreEmpleado, nombreCompleto);
    }

    public void clickEnBotonBuscar() {
        log.debug("Click en boton buscar.");
        click(botonBuscar);
    }

    public Map<String, Object> obtenerDatosDeUsuarioDesdeTabla(Empleado empleadoFake) {
        log.debug("Obtener valor de columnas de usuario encontrado.");
        Map<String, Object> columnasUsuarioEncontrado = new HashMap<>();
        List<List<WebElement>> tablaEmpleados = obtenerFilasDeTablaEmpleados();
        for (List<WebElement> fila : tablaEmpleados) {
            if(getText(fila.get(1)).contentEquals(empleadoFake.getIdEmpleado())) {
                columnasUsuarioEncontrado.put("primerYSegundoNombre", getText(fila.get(2)));
                columnasUsuarioEncontrado.put("apellido", getText(fila.get(3)));
                columnasUsuarioEncontrado.put("cargo", getOptionIndexByText(selectorJobTitle ,getText(fila.get(4))));
                columnasUsuarioEncontrado.put("estadoEmpleo", getOptionIndexByText(selectorEmploymentStatus, getText(fila.get(5))));
                columnasUsuarioEncontrado.put("subUnidad", getOptionIndexByText(selectorSubUnit, getText(fila.get(6))));
                columnasUsuarioEncontrado.put("supervisor", getText(fila.get(7)));
            }
        }
        return columnasUsuarioEncontrado;
    }

    public void marcarCheckboxUsuarioEncontrado(Empleado empleadoFake) {
        log.debug("Marcar checkbox de usuario encontrado.");
        List<List<WebElement>> tablaEmpleados = obtenerFilasDeTablaEmpleados();
        for (List<WebElement> fila : tablaEmpleados) {
            if(getText(fila.get(1)).contentEquals(empleadoFake.getIdEmpleado())) {
                click(fila.get(0).findElement(By.xpath("input[@type='checkbox']")));
            }
        }
    }

    public void clickEnBotonDelete() {
        log.debug("Click en boton delete.");
        click(botonDelete);
    }

    public void confirmarEliminacionDeEmpleado() {
        log.debug("Click en boton confirmar delete.");
        click(botonConfirmarDelete);
    }

    public String obtenerMensajeSinRegistrosEnLaTabla() {
        List<List<WebElement>> tablaEmpleados = obtenerFilasDeTablaEmpleados();
        List<WebElement> fila = tablaEmpleados.get(0);
        return getText(fila.get(0));
    }

}
