package corp.ripley.orangehrm.pageobjects;

import corp.ripley.orangehrm.datasource.EmpleadoDS;
import corp.ripley.orangehrm.model.Empleado;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.ArrayList;
import java.util.List;

public class ReportToPO extends BasePage {

    @FindBy(xpath = "//ul[@id='sidenav']/li/a[text() = 'Report-to']")
    private WebElement menuLateralReportTo;

    @FindBy(id = "btnAddSupervisorDetail")
    private WebElement botonAdd;

    @FindBy(id = "reportto_supervisorName_empName")
    private WebElement campoNombreSupervisor;

    @FindBy(id = "reportto_reportingMethodType")
    private WebElement selectorMetodoAReportar;

    @FindBy(id = "btnSaveReportTo")
    private WebElement botonSaveReportTo;

    @FindBy(id = "sup_list")
    private WebElement tablaSupervisores;

    @FindBy(xpath = "//table[@id='sup_list']/tbody/tr")
    private List<WebElement> filasTablaSupervisores;

    public List<List<String>> obtenerFilasDeTablaSupervisores() {
        log.debug("Obtener datos de tabla de 'Supervisores Asignados'.");
        List<List<String>> tabla = new ArrayList<>();
        waitForElement(tablaSupervisores);
        for(WebElement filaHtml : filasTablaSupervisores) {
            List<String> filaList = new ArrayList<>();
            for(WebElement columnaHtml : filaHtml.findElements(By.xpath("./td"))) {
                filaList.add(columnaHtml.getText());
            }
            tabla.add(filaList);
        }
        return tabla;
    }

    public void irAPaginaReportTo() {
        log.debug("Navegar a la pagina 'Reportar A'.");
        click(menuLateralReportTo);
    }

    public void irAlFormularioAgregarSupervisor() {
        log.debug("Ir al formulario 'Agregar Supervisor'.");
        click(botonAdd);
    }

    public void llenarFormularioAgregarSupervisor(String supervisorName, String reportingMethod, int indexEmpleado) {
        log.debug("Llenar formulario 'Agregar Supervisor'.");
        sendKeys(campoNombreSupervisor, supervisorName);
        selectOptionByText(selectorMetodoAReportar, reportingMethod);
        EmpleadoDS empleadoDS = EmpleadoDS.getInstance();
        Empleado empleado = empleadoDS.empleadoFakeList.get(indexEmpleado);
        empleado.setSupervisor(supervisorName);
    }

    public void guardarFormularioAgregarSupervisor() {
        log.debug("Guardar formulario 'Agregar Supervisor'.");
        click(botonSaveReportTo);
    }

    public List<String> obtenerSupervisorAsignadoEnTabla(String supervisorName) {
        log.debug("Validar supervisor asignado en tabla 'Supervisores Asignados'.");
        List<String> filaSupervisorAgregado = new ArrayList<>();
        List<List<String>> tablaSupervisores = obtenerFilasDeTablaSupervisores();
        for (List<String> fila : tablaSupervisores) {
            if(fila.get(1).contentEquals(supervisorName)) {
                filaSupervisorAgregado = fila;
                break;
            }
        }
        return filaSupervisorAgregado;
    }


}
