@find-employee @regression
Feature: Busqueda de empleado

  Background:
    Given estoy en la pagina login
    And ingreso credenciales correctas de mi usuario
      | username |
      | Admin    |
    And navego a la pagina agregar empleado

  @find-added-employee
  Scenario: Buscar empleado ingresado en el sistema
    Given ingreso un nuevo empleado 2
    And navego a perfil de trabajo
    And edito perfil de trabajo de empleado 2
    And guardo el formulario de trabajo
    And navego a perfil reportar
    And asigno supervisor "John Smith" con metodo "Direct" a empleado 2
    When navego a la pagina Lista Empleados
    And busco por el nombre completo del empleado 2
    Then se muestra empleado 2 en la tabla de resultados

  @find-deleted-employee
  Scenario: Buscar empleado eliminado del sistema
    Given ingreso un nuevo empleado 3
    And navego a perfil de trabajo
    And edito perfil de trabajo de empleado 3
    And guardo el formulario de trabajo
    And navego a perfil reportar
    And asigno supervisor "John Smith" con metodo "Direct" a empleado 3
    And navego a la pagina Lista Empleados
    And busco por el nombre completo del empleado 3
    And se muestra empleado 3 en la tabla de resultados
    When elimino empleado 3
    Then se muestra mensaje "No Records Found"



