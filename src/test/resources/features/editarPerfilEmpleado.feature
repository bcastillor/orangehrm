@edit-employee @regression
Feature: Editar perfil de empleado

  Background:
    Given estoy en la pagina login
    And ingreso credenciales correctas de mi usuario
      | username |
      | Admin    |
    And navego a la pagina agregar empleado

  @edit-employee-job-profile
  Scenario: editar perfil de trabajo
    Given ingreso un nuevo empleado 4
    When navego a perfil de trabajo
    And edito perfil de trabajo de empleado 4
    And guardo el formulario de trabajo
    Then se visualiza la data actualizada de empleado 4

  @assign-employee-supervisor
  Scenario: asignar supervisor en perfil reportar
    Given ingreso un nuevo empleado 5
    When navego a perfil reportar
    And asigno supervisor "John Smith" con metodo "Direct" a empleado 5
    Then se visualiza en la tabla el supervisor "John Smith" y metodo "Direct"



