@add-new-employee @regression
Feature: Agregar nuevo empleado

  Scenario: Ingreso de empleado nuevo
    Given estoy en la pagina login
    And ingreso credenciales correctas de mi usuario
      | username |
      | Admin    |
    When navego a la pagina agregar empleado
    And ingreso un nuevo empleado 0
    Then se despliega pagina con informacion del empleado 0 ingresado
