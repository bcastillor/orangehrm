@login @regression
Feature: Inicio de sesion

  Background:
    Given estoy en la pagina login

  @login-succesful
  Scenario: inicio de sesion exitoso
    When ingreso credenciales correctas de mi usuario
      | username |
      | Admin    |
    Then se despliega pagina de inicio con bienvenida
      | mensaje |
      | Welcome |
    And se despliega menu inicio


  @login-failed
  Scenario Outline: inicio de sesion fallido
    When ingreso credenciales fallidas de mi usuario <username>
    Then se visualiza mensaje credenciales invalidas <mensaje>

    Examples:
      | username | mensaje               |
      | Admin    | "Invalid credentials" |

  @logout
  Scenario: salir de la sesion
    And ingreso credenciales correctas de mi usuario
      | username |
      | Admin    |
    And se despliega pagina de inicio con bienvenida
      | mensaje |
      | Welcome |
    When me deslogueo
    Then se visualiza pagina de inicio